import * as H from 'history';
import {ILocationState } from './locationState.interface';

export interface ProductProps {
    price?: number;
    match?: any;
    from?: any;

    location: H.Location<ILocationState>;
    history: H.History;
}
export interface IProduct {
    _id: string;
    title: string;
    description: string;
    price: number;
    image: string;
}
export interface ProductState {
    products: IProduct[];
    isAllowed: boolean;
    isLoading: boolean;
    message: string;
}