export interface ILocationState {
    errorMessage?: string;
    isRedirected?: boolean;
};