import { ILocationState } from './locationState.interface';
import { match } from "react-router-dom";
import * as H from 'history';

export interface StaticContext {
    statusCode?: number;
}

export interface LoginProps  {
    // by default children below props inherits by Route 
    history: H.History;
    location: H.Location<ILocationState>;
    match: match<any>;
    staticContext?: StaticContext;
}

export interface LoginState {
    isCheckingSession: boolean;
    isLoggedIn: boolean;
}