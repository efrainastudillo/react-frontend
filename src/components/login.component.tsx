import * as React from 'react';
import * as bcrypt from 'bcryptjs';

import Axios, { AxiosResponse, AxiosError } from 'axios';
import { HttpStatus, ResponseMessageCodes, ResponseResult } from '../data/types';

import '../assets/styles/login.scss';
import { LoginProps, LoginState } from '../models/login.interface';
import { Link, Redirect, useParams } from 'react-router-dom';

export class LoginComponent extends React.Component< LoginProps, LoginState > {
    private usernameInput: React.RefObject<HTMLInputElement>;
    private passwordInput: React.RefObject<HTMLInputElement>;

    constructor(props: LoginProps){
        super(props);
        
        this.state = {
            isCheckingSession: true, // while waiting to check the validity of the token
            isLoggedIn: false // once got result, set this var accordingly
        }

        this.usernameInput = React.createRef();
        this.passwordInput = React.createRef();
        this.onLogin = this.onLogin.bind(this);
    }

    // if this component was called from a Redirect component. May contain some 
    wasRedirected() {
        return this.props.location.state !== null && this.props.location.state !== undefined && this.props.location.state.isRedirected;
    }

    //* Called when this component have been mounted in the DOM
    componentDidMount() {
        // const msg = this.wasRedirected()? this.props.location.state.errorMessage : 'No error message yet';
        // console.log(`Login Product state ios : ${msg}`);
        const token = localStorage.getItem('token');
        if(!token){
            this.setState({isCheckingSession: false, isLoggedIn: false});
            return;
        }
        else {
            this.setState({});
        }
    }
    // check whether the token is valid or not
    isLoggedIn() : boolean {
        
        return false;
    }
    // Make a request to the server to validate username and password
    onLogin(event: React.MouseEvent) : void {
        event.preventDefault();
        const credentials = { 
            username: this.usernameInput.current.value,
            password: this.passwordInput.current.value
        }
    
        Axios.post('http://localhost:8080/login', credentials)
        .then( ( _data : AxiosResponse<ResponseResult>) => {
            const {code, message, data} = _data.data;
            console.log(`Data received ${message}`);
            if(_data.status === HttpStatus.OK && code === ResponseMessageCodes.OK ){
                console.log(`Token is : ${data}`);
                localStorage.setItem('token', data);
            }
        })
        .catch( (_error: AxiosError) => {
            console.error(`Error login: ${_error.message}`);
        });
    }

    render() : React.ReactNode {
        if( this.state.isLoggedIn )
        {
            return <Redirect to={{
                pathname: '/products',
                state: {
                    isRedirected: true
                }
            }} />
        }
        else
            return <React.Fragment>
            <div className='row justify-content-md-center'>
                <div className='col col-lg-4'>
                    {this.wasRedirected()? <p className='alert alert-danger' role='alert'> {this.props.location.state.errorMessage} </p> : ''}
                    <h1>Login</h1>
                    <div className='login-padding'>
                        <label htmlFor='username'>Username</label>
                        <input ref={this.usernameInput} id='username' className="form-control" type='text' placeholder='Enter username'></input>
                    </div>
                    <div className='login-padding'>
                        <label htmlFor='password'>Password</label>
                        <input ref={this.passwordInput} id='password' className="form-control" type='password' placeholder='Password'></input>
                    </div>
                    <div className='login-padding login-actions'>
                        <button className='btn btn-primary' type='button' onClick={this.onLogin} >Login</button>
                        <Link to='/register'>Register</Link>
                    </div>
                </div>
            </div>
        </React.Fragment>
    }
}