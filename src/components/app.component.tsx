import * as React from "react";
import { Switch, Route, BrowserRouter, Link } from "react-router-dom";
import { ShopComponent } from "./shop.component";
import { LoginComponent } from "./login.component";
import { ProductComponent } from "./product.component";
import SignupComponent from "./signup.component";

export default class App extends React.Component<{}, {}> {
    constructor(props: {}){
        super(props);
    }
    render() : React.ReactNode {
        return <React.Fragment>
            <BrowserRouter>
                <nav className='navbar navbar-expand-lg navbar-light bg-light'>
                    <a href="#" className='navbar-brand'>My Store</a>
                    <div className='collapse navbar-collapse' id='navbarNav'>
                        <ul className='navbar-nav'>
                            <li className='nav-item active'>
                                <Link className='nav-link' to="/products/1234567890">Products with ID</Link>
                            </li>
                            <li className='nav-item'>
                                <Link className='nav-link' to="/products">Products</Link>
                            </li>
                            <li className='nav-item'>
                                <Link className='nav-link' to="/">Login</Link>
                            </li>
                        </ul>

                    </div>
                </nav>
                <div className='container'>
                    <Switch>
                        <Route path='/' exact render={ (routeProps) => <LoginComponent { ...routeProps }/>} />
                        <Route path='/products' exact render={(routeProps) => <ShopComponent {...routeProps}/> } />
                        <Route path='/products/:id' render={ (routeProps) => <ProductComponent { ...routeProps }/>} />
                        <Route path='/register' render={ (routeProps) => <SignupComponent {...routeProps} /> } />
                    </Switch>
                </div>
            </BrowserRouter>
        </React.Fragment>
    }
}