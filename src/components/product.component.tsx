import * as React from 'react';
import { ProductProps, ProductState } from '../models/product.interface';
import { HttpStatus, ResponseResult } from '../data/types';
import Axios, { AxiosResponse } from 'axios';
import { Redirect } from 'react-router-dom';

import image from '../assets/images/arlopro.jpg';

export class ProductComponent extends React.Component<ProductProps, ProductState> {
    constructor(_props: ProductProps){
        super(_props);

        this.state = {
            message: '',
            isAllowed: false,
            isLoading: true,
            products: null
        };
    }
    wasRedirected() : boolean {
        return this.props.location.state !== null && this.props.location.state !== undefined;
    }
    componentDidMount() {
        // get products from backend
        console.log('product mounted');
        const msg = this.props.location.state? this.props.location.state.errorMessage : 'No error message';
        console.log(`Product Product state ios : ${ msg }`);
        
        if(this.state.isLoading) {
            Axios.interceptors.request.use( config => {
                const token = localStorage.getItem('token'); 
                if( token )
                    config.headers.authorization = `Bearer ${token}`;

                return config;
            });
            Axios.get('http://localhost:8080/products')
            .then( (_result: AxiosResponse<ResponseResult>) => {
                if(_result.data.code === HttpStatus.OK)
                {
                    this.setState({isLoading: false, isAllowed: true, products: _result.data.data });
                    return;
                }
                else if(_result.data.code === HttpStatus.BAD_AUTHORIZATION )
                {
                    this.setState({isLoading: true, isAllowed: false});
                    return;
                }
    
                console.log(`Data received : ${_result.data.message}`);
            })
            .catch(_error => {
                this.setState({isLoading: false, isAllowed: false, products: null, message: _error.message});
                console.error(`Error retrieving products data. ${_error}`);
            });
        }
    }

    render () : React.ReactNode {
        // const { id } = this.props.match.params
        if(this.state.isLoading ){
            return <div>Loading data...</div>
        }
        else
        {
            if(this.state.isAllowed && this.state.products != null) {

                return <React.Fragment>
                    <div className='row'>
                    { this.state.products.map( (item, index)=> {
                            return (
                                <div className="col-md-3" key={index}>
                                    <div className="card">
                                        <img className="card-img-top" src={image} alt="Card image cap" />
                                        <div className="card-body">
                                            <h5 className="card-title">{item.title}</h5>
                                            <p className="card-text">{item.description}</p>
                                            <a href="#" className="btn btn-primary">Add to Cart</a>
                                        </div>
                                    </div>
                            </div>)
                        })}
                    </div>
                        
                </React.Fragment>
            }
            else{
                return <Redirect to={
                    {
                        pathname: '/', // login
                        state: { 
                            isRedirected: true,
                            errorMessage: this.state.message 
                        }
                    }
                } />
            }
            
        }
    }
}