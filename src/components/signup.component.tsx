import * as React from 'react';

import {SignupProps, SignupState} from '../models/signup.interface';
import '../assets/styles/signup.scss';
import Axios from 'axios';

export default class SignupComponent extends React.Component<SignupProps, SignupState> {
    private usernameInput: React.RefObject<HTMLInputElement>;
    private passwordInput: React.RefObject<HTMLInputElement>;
    private emailInput: React.RefObject<HTMLInputElement>;

    constructor(props : SignupProps){
        super(props);
        this.usernameInput = React.createRef();
        this.passwordInput = React.createRef();
        this.emailInput = React.createRef();

        this.onRegister = this.onRegister.bind(this);
    }

    componentDidMount(){}

    onRegister() {
        const data = {
            username: this.usernameInput.current.value,
            password: this.passwordInput.current.value,
            email: this.emailInput.current.value
        }

        Axios.post('http://localhost:8080/signup', data)
        .then( res => {
            console.log(`result: ${JSON.stringify(res)}`);
        })
        .catch(error => {
            console.log(`result error: ${JSON.stringify(error)}`)
        });
        console.info('Click on Register');
    }
    render() : React.ReactNode {
        return <React.Fragment>
            <div className='row justify-content-md-center'>
                <div className='col col-lg-4'>
                    <h1>Registration</h1>
                    <div className='signup-padding'>
                        <label htmlFor='usermame'>Username:</label>
                        <input ref={this.usernameInput} id='username' className="form-control"  placeholder='Username' type='text'/>
                    </div>
                    <div className='signup-padding'>
                        <label htmlFor='email'>Email:</label>
                        <input ref={this.emailInput} id='email' className="form-control" placeholder='email' type='text'/>
                    </div>
                    <div className='signup-padding'>
                        <label htmlFor="password">Password:</label>
                        <input ref={this.passwordInput} id='password' className="form-control" type="password" placeholder='Password'/>
                    </div>
                    <div className='signup-padding'>
                        <button className='btn btn-primary' type='button' onClick={this.onRegister}>Sign Up</button>
                    </div>
                </div>
            </div>
        </React.Fragment>
    }
}