import * as React from 'react';
export interface HeaderProps{}
export interface HeaderState{}

export class HeaderComponent extends React.Component<HeaderProps, HeaderState>{
    constructor(props: HeaderProps){
        super(props);
    }

    render = () => 
    <React.Fragment>
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Products</a></li>
        </ul>
    </React.Fragment>
}