import * as React from "react";

export interface ShopProps { 
    storeName: string; 
    storeOwner: string; 
}
export interface Product { 
    title: string;
    description: string;
    image: string;
    price: number;
}

// 'HelloProps' describes the shape of props.
// State is never set so we use the '{}' type.
export class ShopComponent extends React.Component<{}, {}> {
    
    /**
     * When a component is mounted this is trigger...
     */
    componentDidMount() {
        // fetch('http://localhost:8080/products')
        // .then(_response => {
        //     console.log(`Data received from server: ${_response}`);
        //     if( _response.status === 401 )
        //     {

        //     }
        //     // this.setState( _response.json(). );
        // })
        // .catch( _error => {
        //     console.log(`Error fetching data from server: ${_error}`);
        // });
        console.log('shop mounted!!');
        
    }

    render(): React.ReactNode {
        return  <React.Fragment>
                    <h1>SHop</h1>
                </React.Fragment>
    }        
};