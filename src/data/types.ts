export const DataTypes = { 
    PRODUCTS: "products", 
    CATEGORIES: "categories"
}
export const ActionTypes = { 
    DATA_LOAD: "data_load"
}
export enum HttpStatus {
    OK = 200,
    BAD_AUTHORIZATION = 401,
    RESOURCE_NOT_FOUND = 404
}
export interface ResponseResult {
    code: number;
    message: string;
    data?: any;
}
export enum ResponseMessageCodes {
    OK = 200,
    USER_NOT_FOUND = 450
}