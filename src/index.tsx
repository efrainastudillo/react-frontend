import * as React from "react";
import * as ReactDOM from "react-dom";
import 'bootstrap';

// import { Data } from "./models/data";
import App from "./components/app.component";

ReactDOM.render(
    <App />,
    document.getElementById("root")
);