# Setup a custom React project using Typescript and WebPack. 

## Create main directories.
1. **src folder**: This directory is where source (Typescript code) is located
2. **dist folder**: This directory is where compiled files (Using Webpack) will be goint to be saved

## Install Webpack and Webpack Server
Webpack is a tool that will bundle your code and optionally all of its dependencies into a single `.js` file.
Let’s now add React and React-DOM, along with their declaration files, as dependencies to your package.json file:

`npm install --save-dev webpack webpack-cli`
`npm install --save-dev webpack-dev-server html-webpack-plugin`

Here html-webpack-plugin will use your custom index.html that will be rendered by webpack-dev-server
Please note that if you don’t pass any param in `new HTMLWebpackPlugin()` , then the html-webpack-plugin plugin will generate an HTML5 file for you that includes all your webpack bundles in the body using script tags.

## Install React and Typescript
`npm install --save-dev typescript`
`npm install --save react react-dom`
`npm install --save-dev @types/react @types/react-dom`

That `@types/` prefix means that we also want to get the declaration files for React and React-DOM. Usually when you import a path like "react", it will look inside of the react package itself; however, not all packages include declaration files, so TypeScript also looks in the @types/react package as well. You’ll see that we won’t even have to think about this later on.

Next, we’ll add development-time dependencies on the ts-loader and source-map-loader.

## Add ts-loader and source-map-loader
`npm install --save-dev ts-loader source-map-loader`

## Use Images with React and Webpack
How to set up Webpack to use images as assets for your application. Essentially, there is not much in Webpack to include your desired images for your web application. First, put your image files into one folder of your projects application. For instance, your src/ folder may have a folder assets/ which has a folder images/.

```- src/
--- assets/
----- images/
------- myimage.jpg
```

Second, install a commonly used Webpack loader to include the images into your bundling process:
`npm install --save-dev url-loader`.

And third, include the new loader in your Webpack configuration:

```module.exports = {
  ...
  module: {
    rules: [
      ...
      {
        test: /\.(jpg|png)$/,
        use: {
          loader: 'url-loader',
        },
      },
    ],
  },
  ...
};```