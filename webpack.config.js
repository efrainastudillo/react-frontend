const path = require('path');

module.exports = {
    mode: "development",
    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",
    entry: {
        app: './src/index.tsx'
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".jpg"],
        modules: ["src", "node_modules"],
    },
    devServer: {
        compress: false,
        port: 3000
    },
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "ts-loader"
                    }
                ]
            },
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            },
            {
                test: /\.(jpg|png|gif|svg)$/i,
                use:[
                    'url-loader',
                    'file-loader'
                    ]
            },

            {
                // .sass or .scss file extensions allowed
                test: /\.s[ac]ss$/i,
                use: [
                  // Creates `style` nodes from JS strings
                  'style-loader',
                  // Translates CSS into CommonJS
                  'css-loader',
                  // Compiles Sass to CSS
                  {
                    loader: 'sass-loader',
                    options: {
                      // Prefer `dart-sass`
                      implementation: require('sass'),
                    },
                  },
                ],
            }
        ]
    },
    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    },
    watch: true,
    watchOptions: {
        ignored: ['./**/*.js', 'node_modules/**']
    },
    performance: {
        hints: process.env.NODE_ENV === 'production' ? "warning" : false
      },
};